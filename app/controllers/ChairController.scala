package controllers

import javax.inject.{Inject, Singleton}
import play.api.data.validation.Constraints._
import models._
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc.{MessagesAbstractController, MessagesControllerComponents}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@Singleton
class ChairController @Inject()(cc: MessagesControllerComponents,
                                db:Repository
                               ) extends MessagesAbstractController(cc) {

  val chairForm: Form[CreateChairForm] = Form {
    mapping(
      "chairName" -> nonEmptyText
    )(CreateChairForm.apply)(CreateChairForm.unapply)
  }

  val addStudentToChairForm: Form[AddStudentToChairForm] = Form {
    mapping(
      "chair_id" -> number,
      "student_id" -> number
    )(AddStudentToChairForm.apply)(AddStudentToChairForm.unapply)
  }

  def createChairGetMethod = Action { implicit request =>
    Ok(views.html.chairs.create_chair(chairForm))
  }

  def createChairPostMethod = Action { implicit request =>
    val binding = chairForm.bindFromRequest.get
    db.createChair(binding.chairName)
    Redirect("/")
  }

  def chairInfo(id: Int) = Action {implicit request =>
    val chair = Await.result(db.getChairById(id), Duration.Inf)
    chair match {
      case None => BadRequest(s"кафедра не существует")
      case _ => {
        val students = Await.result(db.getChairStudents(id), Duration.Inf)
        Ok(views.html.chairs.info(chair.head, students))
      }
    }
    //Ok("ok")
  }

  def deleteChair(id: Int) = Action {implicit request =>
    if (Await.result(db.deleteChair(id),Duration.Inf) > 0)
      Redirect("/")
    else
      BadRequest("Невозможно удалить кафедру")
  }

  def addStudentToChairGetMethod(chairId: Int) = Action {implicit request =>
    val students = Await.result(db.studentsList(), Duration.Inf)
    val chair = Await.result(db.getChairById(chairId), Duration.Inf)
    chair match {
      case None => BadRequest(s"нет кафедры с id=$chairId")
      case _ => {
        Ok(views.html.chairs.add_student_to_chair(chair.head,students,addStudentToChairForm))
      }
    }
  }

  def addStudentToChairPostMethod(chairId: Int) = Action {implicit request =>
    val binding = addStudentToChairForm.bindFromRequest.get
    val student = Await.result(db.getStudent(binding.studentId), Duration.Inf)
    student match {
      case None => BadRequest("студент не существует")
      case _ => {
        student.head.ChairId match {
          case None => {
            Await.result(
              db.addStudentToChair(student.head.StudentId, chairId),
              Duration.Inf
            )
            Redirect(s"/chairs/get/$chairId")
          }
          case _ => BadRequest("студент уже прикреплён к другой кафедре")
        }
      }
    }
    //Ok("")
  }

  def deleteStudentFromChair(chairId: Int, studentId: Int) = Action {implicit request =>
    val deleteResult = db.deleteStudentFromChair(chairId, studentId)
    deleteResult match {
      case Left(_) => Redirect(s"/chairs/get/$chairId")
      case Right(message) => BadRequest(message)
    }
  }

  def editChairGetMethod(chairId: Int) = Action {implicit request =>
    val chair = Await.result(db.getChairById(chairId), Duration.Inf)
    chair match {
      case None => BadRequest("Кафедра не существует")
      case _ => Ok(views.html.chairs.edit_chair(chair.head, chairForm))
    }
  }

  def editChairPostMethod(chairId: Int) = Action {implicit request =>
    val binding = chairForm.bindFromRequest.get
    Await.result(db.updateChair(Chair(chairId,binding.chairName)), Duration.Inf)
    Redirect(s"/chairs/get/$chairId")
  }
}

case class CreateChairForm(chairName: String)
case class AddStudentToChairForm(chairId: Int, studentId: Int)