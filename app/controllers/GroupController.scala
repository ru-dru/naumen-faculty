package controllers

import javax.inject.{Inject, Singleton}
import models._
import org.postgresql.util.PSQLException
import play.api.mvc._
import play.api.data.Form
import play.api.data.Mapping
import play.api.data.Forms._
import play.api.i18n._
import play.api.libs.json.Json
import play.api.Play.current
import play.api.i18n.Messages.Implicits._
import play.api.mvc._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

@Singleton
class GroupController @Inject()(cc: MessagesControllerComponents,
                                db:Repository
                               ) extends MessagesAbstractController(cc) {

  val groupForm: Form[CreateGroupForm] = Form {
    mapping(
      "id" -> number(100, 999)
    )(CreateGroupForm.apply)(CreateGroupForm.unapply)
  }


  def index(groupIndex: Int) = Action { implicit request:Request[AnyContent]=>
      val group = Await.result(db.getGroup(groupIndex),Duration.Inf)
      group match {
        case None => BadRequest("Группа не существует")
        case _ => Ok(groupIndex.toString())
    }
  }

  def createGroupGetMethod = Action { implicit request =>
    Ok(views.html.groups.create_group(groupForm))
  }

  def createGroupPostMethod = Action { implicit request =>
    val binding = groupForm.bindFromRequest.get
    db.createGroup(Group(binding.groupId))
    Redirect("/")
  }

  def groupInfo(id: Int) = Action { implicit request =>
    val group = Await.result(db.getGroup(id), Duration.Inf).headOption
    group match {
      case None => BadRequest("Группа не существует")
      case _ => {
        val students = Await.result(db.getGetGroupStudents(id), Duration.Inf)
        Ok(views.html.groups.info(group.head, students)(db))
      }
    }
  }

  def deleteGroup(groupId: Int) = Action { implicit request =>
    try {
      if (Await.result(db.deleteGroup(groupId), Duration.Inf) > 0)
        Redirect("/")
      else
        BadRequest("Невозможно удалить группу")
    } catch {
      case error: PSQLException => BadRequest(error.getMessage)
    }
  }

  def editGroupGetMethod(groupId: Int) = Action {implicit request =>
    val group = Await.result(db.getGroup(groupId), Duration.Inf)
    group match {
      case None => BadRequest("Группа не существует")
      case _ => Ok(views.html.groups.edit_group(groupId, groupForm))
    }
  }

  def editGroupPostMethod(oldId: Int) = Action {implicit request =>
    val binding = groupForm.bindFromRequest.get
    Await.result(db.updateGroup(oldId, binding.groupId), Duration.Inf)
    Redirect("/")
  }
}

case class CreateGroupForm(groupId: Int)