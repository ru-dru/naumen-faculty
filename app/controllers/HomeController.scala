package controllers

import javax.inject._
import models.{Repository, Student}
import play.api._
import play.api.db.slick.DatabaseConfigProvider
import play.api.mvc._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(cc: ControllerComponents,
                               db:Repository
                              ) extends AbstractController(cc) {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>

    Ok(views.html.index(
      Await.result(db.groupsList(), Duration.Inf),Await.result(db.chairsList(), Duration.Inf)
    )
    )
  }
}
