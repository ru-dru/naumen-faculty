package controllers

import javax.inject.{Inject, Singleton}
import models._
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc.{AbstractController, ControllerComponents, MessagesAbstractController, MessagesControllerComponents}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@Singleton
class StudentController @Inject()(cc: MessagesControllerComponents,
                               db:Repository
                              ) extends MessagesAbstractController(cc) {

  val addStudentForm: Form[AddStudentForm] = Form {
    mapping(
      "first_name" -> nonEmptyText,
      "last_name" -> nonEmptyText,
      "patronymic" -> nonEmptyText,
      "group_id" -> number,
      "chair_id" -> optional(number)
    )(AddStudentForm.apply)(AddStudentForm.unapply)
  }


  def getStudent(studentId: Int) = Action {request =>
    val student = Await.result(db.getStudent(studentId), Duration.Inf)
    student match {
      case None => BadRequest("Студент не существует")
      case _ => Ok(views.html.students.student_info(student.head)(db))
    }
  }

  def deleteStudent(studentId: Int) = Action {implicit request =>
    if (Await.result(db.deleteStudent(studentId),Duration.Inf) > 0)
        Ok("Студент отчилен")
    else
      BadRequest("Студент не существует")
  }

  def createStudentGetMethod(groupId: Int) = Action { implicit request =>
    Ok(views.html.students.create_student(
      groupId,
      Await.result(db.groupsList(), Duration.Inf),
      Await.result(db.chairsList(), Duration.Inf),
      addStudentForm))
  }

  def createStudentPostMethod(groupId: Int) = Action { implicit request =>
    val binding = addStudentForm.bindFromRequest.get
    Await.result(db.createStudent(Student(0,binding.FirstName,binding.LastName,binding.Patronymic,binding.GroupId,binding.ChairId)), Duration.Inf)
    Redirect(s"/groups/get/$groupId")
  }

  def editStudentGetMethod(studentId: Int) = Action {implicit request =>
    val student = Await.result(db.getStudent(studentId), Duration.Inf)
    student match {
      case None => BadRequest("Студент не сущетствует")
      case _ => Ok(views.html.students.edit_student(
        student.head,
        Await.result(db.groupsList(), Duration.Inf),
        Await.result(db.chairsList(), Duration.Inf),
        addStudentForm
        )
      )
    }
  }

  def editStudentPostMethod(studentId: Int) = Action {implicit request =>
    val binding = addStudentForm.bindFromRequest.get
    Await.result(db.updateStudent(Student(
      studentId,
      binding.FirstName,
      binding.LastName,
      binding.Patronymic,
      binding.GroupId,
      binding.ChairId
    )),  Duration.Inf)
    Redirect(s"/students/get/$studentId")
  }

}

case class AddStudentForm(FirstName: String, LastName: String, Patronymic: String, GroupId: Int, ChairId: Option[Int])