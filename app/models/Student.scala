package models

case class Student (StudentId: Int, FirstName: String, LastName: String, Patronymic:String, GroupId: Int, ChairId: Option[Int])