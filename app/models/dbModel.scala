package models
import java.util.Locale.Category

import slick.jdbc.PostgresProfile.api._
import javax.inject.{Inject, Singleton}
import org.checkerframework.checker.units.qual.g
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

class Repository @Inject() (dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext){

  private val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  private class Students(tag: Tag) extends Table[Student](tag, "students") {
    def StudentId = column[Int]("student_id", O.PrimaryKey, O.AutoInc)
    def FirstName = column[String]("first_name")
    def SecondName = column[String]("second_name")
    def Patronymic = column[String]("patronymic")
    def GroupId = column[Int]("group_id")
    def ChairId = column[Option[Int]]("chair_id")

    def GroupForeignKey =
      foreignKey("group_fk", GroupId, students)(_.StudentId, onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)

    def ChairForeignKey = foreignKey(
      "chair_fk", ChairId, students
    )(_.StudentId.?, onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.SetNull)

    def * = (StudentId, FirstName, SecondName, Patronymic, GroupId, ChairId) <> ((Student.apply _).tupled, Student.unapply)
  }

  private class Groups(tag: Tag) extends Table[Group](tag, "groups") {
    def group_id = column[Int]("group_id", O.PrimaryKey)
    def * = group_id <> ((Group.apply _), Group.unapply)
  }

  private class Chairs(tag: Tag) extends Table[Chair](tag, "chairs") {
    def chair_id = column[Int]("chair_id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("chair_name")
    def * = (chair_id, name) <> ((Chair.apply _).tupled, Chair.unapply)
  }

  private val students = TableQuery[Students]
  private val groups = TableQuery[Groups]
  private val chairs = TableQuery[Chairs]

  def createStudent(student: Student) = db.run {
    students += Student(0, student.FirstName, student.LastName,student.Patronymic, student.GroupId, student.ChairId)
  }

  def getStudent(id: Int):Future[Option[Student]] = db.run {
    students.filter(_.StudentId === id).result.headOption
  }

  def deleteStudent(id: Int) = db.run {
    students.filter(_.StudentId === id).delete
  }

  def updateStudent(student: Student) = db.run {
    students.filter(_.StudentId === student.StudentId).update(student)
  }

  def studentsList():Future[Seq[Student]] = db.run {
    students.result
  }

  def addStudentToChair(studentId: Int, chairId: Int) = db.run {
    val q = for {student <- students if student.StudentId === studentId } yield student.ChairId
    q.update(Option(chairId))
  }


  def deleteStudentFromChair(chairId: Int, studentId: Int):Either[Future[Int], String] = {

    /**
      * 1. найти студента
      * 2. если студент есть, совпадают ли кафедры
      * 3. если совпадают, занулить
      */
    val st = Await.result(getStudent(studentId), Duration.Inf)
    st match {
      case None => Right("Студент не существует")
      case _ => {
        if (st.head.ChairId != None && st.head.ChairId.head == chairId) {
          Left(db.run {
            val q = for {student <- students if student.StudentId === studentId} yield student.ChairId
            q.update(None)
          })
        }
        else {
          Right("Студент прикреплён к другой кафедре")
        }
      }
    }
  }

  def chairsList():Future[Seq[Chair]] = db.run {
    chairs.result
  }

  def groupsList():Future[Seq[Group]] = db.run {
    groups.result
  }

  def createGroup(newGroup: Group)= db.run {
    groups += newGroup
  }

  def deleteGroup(id: Int) = db.run {
    groups.filter(_.group_id === id).delete
  }

  def getGroup(id: Int):Future[Option[Group]] = db.run {
    groups.filter(_.group_id === id).result.headOption
  }

  def updateGroup(old_id: Int, new_id: Int) = db.run {
    val q = for { g <- groups if g.group_id === old_id } yield g.group_id
    q.update(new_id)
  }

  def getGetGroupStudents(groupId: Int):Future[Seq[Student]] = db.run {
    students.filter(_.GroupId === groupId).result
  }

  def deleteChair(chairId: Int):Future[Int] = db.run {
    chairs.filter(_.chair_id === chairId).delete
  }

  def createChair(name: String): Unit = db.run {
    (chairs.map(c => c.name) returning chairs.map(_.chair_id) into ((name, newId) => Chair(newId, name)) += (name))
  }

  def getChairById(id: Int): Future[Option[Chair]] = db.run {
    chairs.filter(_.chair_id === id).result.headOption
  }

  def getChairStudents(chairId: Int): Future[Seq[Student]] = db.run {
    students.filter(_.ChairId === chairId).result
  }

  def updateChair(chair: Chair) = db.run {
    chairs.filter(_.chair_id === chair.ChairId).update(chair)
  }
}