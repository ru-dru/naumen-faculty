
-- Role: dean

CREATE ROLE dean WITH
    LOGIN
    SUPERUSER
    INHERIT
    PASSWORD 'dean'
    CREATEDB
    CREATEROLE
    NOREPLICATION;

COMMENT ON ROLE dean IS 'Декан';

CREATE DATABASE faculty
    WITH
    OWNER = dean
    ENCODING = 'UTF8'
    
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;



