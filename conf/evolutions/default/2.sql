create table groups
(
    group_id integer not null
        constraint groups_pk
            primary key
);

alter table groups
    owner to dean;

create unique index groups_group_id_uindex
    on groups (group_id);

create table chairs
(
    chair_id   serial not null
        constraint chairs_pk
            primary key,
    chair_name varchar(500)
);

alter table chairs
    owner to dean;

create unique index chairs_chair_id_uindex
    on chairs (chair_id);

create table students
(
    student_id  serial       not null
        constraint students_pk
            primary key,
    first_name  varchar(100) not null,
    second_name varchar(100) not null,
    patronymic  varchar(100),
    group_id    integer
        constraint group_fk
            references groups
            on update cascade on delete restrict,
    chair_id    integer
        constraint chair_fk
            references chairs
            on delete set null
);

alter table students
    owner to dean;

create unique index students_student_id_uindex
    on students (student_id);

