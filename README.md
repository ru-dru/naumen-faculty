# Абстрактный факультет

*Выполнено в качестве тестового задания от компании Naumen.*

Информационная система факультета позволяет управлять группами и кафедрами: создавать/изменять/удалять.

Как и на любом факультете здесь учатся студенты, которых можно "зачислять"/"отчислять"/изменять информацию о них. Каждый студент обязательно прикреплён к какой-то группе. И, возможно,
к одной из кафедр.

## Запуск
Для работы приложения необходимы следующие компоненты:
* Scala (использовалась версия 2.13)
* PostgreSQL(использовалась версия 11.5)
* sbt (использовалась версия 1.2.8)

В начале нужно создать базу данных и пользователя. Механизм **evolutions** я не смог заставить работать (хотя при первом запуске в браузере показывается сообщение с кнопкой применения скрипта пустого), поэтому запустить скрипты нужно вручную.
**Перед запуском необходимо прописать путь к базе данных в файле /conf/application.conf в поле slick.dbs.default.db.url**

**Скрипты нужно запускать от имени пользователя, обладающего правами создания пользователей и баз данных, например, от имени пользователя postgres**

### Linux

В командной строке выполнить следующие команды:
```bash
cd /tmp
git clone https://gitlab.com/ru-dru/naumen-faculty
cd naumen-faculty/conf/evolutions/default
psql -U postgres -f 1.sql
psql -U postgres -f 2.sql -d -faculty
cd ../../..
sbt run
```
Зайти в веб браузере по адресу http://localhost:9000

Готво

### Windows
1. **расположение файла psql.exe может не совпадать с приведенным примером**
2. **Возможно необходимо добавление некоторых путей в переменную среды PATH**

Выполнить в командной строке следующие действия:
```bash
cd <удобный каталог>
git clone https://gitlab.com/ru-dru/naumen-faculty
cd naumen-faculty/conf/evolutions/default
"c:\Program Files (x86)\pgAgent\bin\psql.exe -U postgres -f 1.sql"
"c:\Program Files (x86)\pgAgent\bin\psql.exe -U postgres -f 2.sql -d faculty"
cd ..\..\..
sbt run
```
Зайти в веб браузере по адресу http://localhost:9000

Готво

## Тестирование
### Unit тесты
Подготовить модульные тесты я уже не успел -- Scala абсолютно новый для меня язык и вся экосистема, соотвественно, тоже.
Пришлось потратить немало времени чтобы во всём разобраться. Времени чтобы разобраться с фрейворком тестирования уже
не осталось.

Оформил бы я их следующим образом:
* завёл бы отдельные настройки для тестовой базы
* заполнил бы базу тестовыми данными
* описал бы тесты:
    - по чтению данных
    - по добавлению данных
    - по изменению данных
    - по удалению данных
    
### Ручное тестирование
Добавили пару групп и кафедру

![1.png](images/1.png "Logo Title Text 1")
![1.png](images/2.png "Logo Title Text 1")

Добавляем студентов в группу
![1.png](images/3.png "Logo Title Text 1")
![1.png](images/4.png "Logo Title Text 1")
![1.png](images/5.png "Logo Title Text 1")

Название кафедры можно изменять
![1.png](images/6.png "Logo Title Text 1")

Попытка удалить группу завершается ошибкой, т.к. в ней ещё есть студенты (кафедру же можно удалять,
в этом случае у прикреплённых к ней студентов поле ChairId становится пустым)
![1.png](images/7.png "Logo Title Text 1")

На кафедру можно добавлять студентов. При этом если студент уже прикреплён к другой кафедре,
на эту его добавить не получится
![1.png](images/8.png "Logo Title Text 1")
![1.png](images/9.png "Logo Title Text 1")

Информацию о студенте можно изменять (ФИО, переводить в другую группу, менять кафедру)
![1.png](images/10.png "Logo Title Text 1")

## Что бы хотелось улучшить
Класс dbModel разросся и получился большой и не структурированный, наверно лучше было бы объединить операции по сущносям.

В контроллерах есть куски похожего кода. Возможно их лучше было бы описать в виде параметризованных методов.  

## Итог
Это было моё первое знакомство с языком Scala. В целом мне понравилось. Опыт разработаки подобных веб приложений у меня уже был,
это помогло быстрее освоиться.

Задание выполнено, требуемый функционал реализован.